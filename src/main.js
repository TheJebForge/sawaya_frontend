import 'es6-promise/auto'

import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import Sawaya from './plugins/sawaya_http'
import Utils from './plugins/utils'
import i18n from './i18n'

Vue.use(Vuex);
Vue.use(Sawaya);
Vue.use(Utils);

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')