import axios from 'axios';

export default {
    install(Vue) {
        let sawaya_obj = {

        };

        let base_url = "http://127.0.0.1:8020";

        sawaya_obj.set_session = (session, callback) => {
            sawaya_obj.session_key = session;

            axios.get(`${base_url}/session`, {
                headers: {
                    'Session': session
                }
            }).then(response => {
                sawaya_obj.session = response.data;
                
                if(callback)
                    callback(true);
            }).catch(() => {
                if(callback)
                    callback(false);
            });
        }

        sawaya_obj.get_guild_info = (guild_id, callback) => {
            axios.get(`${base_url}/info?guild_id=${guild_id}`, {
                headers: {
                    'Session': sawaya_obj.session_key
                }
            }).then(response => {              
                if(callback)
                    callback(response.data);
            }).catch(() => {
                if(callback)
                    callback(null);
            });
        }

        Vue.sawaya = sawaya_obj;

        Vue.mixin({
            created() {
                this.$sawaya = Vue.sawaya;
            }
        })
    }
};