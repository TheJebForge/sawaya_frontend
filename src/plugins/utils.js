export default {
    install(Vue) {
        Vue.mixin({
            created() {
                this.supported_modules = {
                    mover_module: {
                        name: "Mover Module",
                        icon: "mdi-arrow-left-right",
                        route: "/panel/mover",
                    }
                };

                this.panel_check_if_accessible = (codename) => {
                    let item = window.localStorage.getItem("selected-guild");
                    if(item) {
                        item = JSON.parse(item);
                        if(!item.modules.includes(codename)) {
                            this.$router.push("/panel");
                            return false;
                        } else {
                            return true;
                        }
                    }
                };

                this.try_login = (callback) => {
                    if(!this.$sawaya.session) {
                        let session_id = window.localStorage.getItem("session");
        
                        if(!session_id) {
                            this.$router.push({
                                name: "Login"
                            });
                        } else {
                            this.$sawaya.set_session(session_id, (result) => {
                                if(result)
                                    callback(true);
                                else {
                                    this.$router.push({
                                        name: "Login"
                                    });
                                }
                            })
                        }
                    } else {
                        if(callback)
                            callback(true);
                    }
                }
            }
        })
    }
}