import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        themes: {
            dark: {
                primary: '#2f3136',
                secondary: '#5865f2',
                accent: '#82B1FF',
                error: '#ed4245',
                info: '#82B1FF',
                success: '#46c46e',
                warning: '#faa81a',
                background: '#36393f',
            },
        },
    },
});
