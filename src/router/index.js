import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Index',
    component: () => import('../views/CookieCheck.vue')
  },
  {
    path: '/select',
    name: 'Guild Selection',
    component: () => import('../views/GuildSelection.vue')
  },
  {
    path: '/panel',
    component: () => import('../views/MainPanel.vue'),
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: () => import('../modules/Dashboard.vue'),
      },
      {
        path: 'mover',
        name: 'Mover',
        component: () => import('../modules/Mover.vue'),
      },
      
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
